import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeOwnerSigninComponent } from './home-owner-signin.component';

describe('HomeOwnerSigninComponent', () => {
  let component: HomeOwnerSigninComponent;
  let fixture: ComponentFixture<HomeOwnerSigninComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeOwnerSigninComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeOwnerSigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
