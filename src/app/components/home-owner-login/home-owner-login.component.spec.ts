import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeOwnerLoginComponent } from './home-owner-login.component';

describe('HomeOwnerLoginComponent', () => {
  let component: HomeOwnerLoginComponent;
  let fixture: ComponentFixture<HomeOwnerLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeOwnerLoginComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeOwnerLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
