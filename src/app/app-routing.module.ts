import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { BusinessLoginComponent } from './components/business-login/business-login.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { HomeOwnerLoginComponent } from './components/home-owner-login/home-owner-login.component';
import { LandingComponent } from './components/landing/landing.component';
import { LayoutComponent } from './components/layout/layout.component';
import { SearchPropertyComponent } from './components/search-property/search-property.component';
import { SignupComponent } from './components/signup/signup.component';

const routes: Routes = [  
  {
    path: '',
    component: LayoutComponent,
    children: [      
      {
        path: '',
        component: LandingComponent
      },
      {
        path: 'business-login',
        component: BusinessLoginComponent
      },
      {
        path: 'landing',
        component: LandingComponent
      },
      {
        path: 'home-owner-login',
        component: HomeOwnerLoginComponent
      },
      {
        path: 'aboutus',
        component: AboutusComponent
      },
      {
        path: 'contactus',
        component: ContactusComponent
      },
      {
        path: 'signup',
        component: SignupComponent
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent
      },
      {
        path: 'search-property',
        component: SearchPropertyComponent
      },
    ]
    }
    ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
