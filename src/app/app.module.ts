import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from "@ag-grid-community/angular";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { CarouselComponent } from './common/carousel/carousel.component';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { BusinessComponent } from './components/business/business.component';
import { BusinessSignupComponent } from './components/business-signup/business-signup.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { PropertyRegistrationComponent } from './components/property-registration/property-registration.component';
import { HomeOwnerSigninComponent } from './components/home-owner-signin/home-owner-signin.component';
import { PaymentComponent } from './components/payment/payment.component';
import { SearchPropertyComponent } from './components/search-property/search-property.component';
import { DocsUploadComponent } from './components/docs-upload/docs-upload.component';
import { BusinessLoginComponent } from './components/business-login/business-login.component';
import { HomeOwnerLoginComponent } from './components/home-owner-login/home-owner-login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    LandingComponent,
    LoginComponent,
    SignupComponent,
    CarouselComponent,
    AboutusComponent,
    BusinessComponent,
    BusinessSignupComponent,
    ContactusComponent,
    ForgotPasswordComponent,
    PropertyRegistrationComponent,
    HomeOwnerSigninComponent,
    PaymentComponent,
    SearchPropertyComponent,
    DocsUploadComponent,
    BusinessLoginComponent,
    HomeOwnerLoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    AgGridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
